'use strict';

/**
 * Normalize print
 */
function log(logObj) {
  const date = new Date().toISOString();
  const level = logObj.level.toUpperCase();

  // eslint-disable-next-line no-console
  console[logObj.level](`[${date}] ${level}`, ...logObj.args);
}

/**
 * A layer for log()
 */
function layer(level) {
  return (...args) => log({ level, args });
}

const logger = {
  trace: layer('trace'),
  debug: layer('debug'),
  info: layer('info'),
  warn: layer('warn'),
  error: layer('error'),
  fatal: layer('error'),
};

module.exports = logger;

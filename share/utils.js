'use strict';

/**
 * url 模板
 * @example:
 * urlTemplate('/api/:foo/:bar', { foo: 1, bar: 2 }) => '/api/1/2'
 */
exports.urlTemplate = function(url, params) {
  return url.replace(/:([a-z]+)/gi, (_m, p1) => {
    if (!params[p1]) {
      throw new Error(`url parameter not find: ${url}`);
    }

    return params[p1];
  });
};

function isArray(arr) { return arr && Object.prototype.toString.call(arr) === '[object Array]'; }
function isObject(obj) { return obj && Object.prototype.toString.call(obj) === '[object Object]'; }

exports.isObject = isObject;

exports.isEmpty = function(params) {
  if (!params) return true;

  if (isArray(params)) {
    return !params.length;
  }

  if (isObject(params)) {
    return !Object.keys(params).length;
  }

  return !!params;
};

exports.formatReq = function(logObj) {
  let format = '-> %requestId %method API %url';

  format = format.replace(/%requestId/g, logObj.requestId);
  format = format.replace(/%method/g, logObj.method.toUpperCase());
  format = format.replace(/%url/g, logObj.url);

  return format;
};


exports.formatRes = function(logObj) {
  let format = '<- %requestId %method API %url %status %elapsedTime';

  format = format.replace(/%requestId/g, logObj.requestId);
  format = format.replace(/%method/g, logObj.method.toUpperCase());
  format = format.replace(/%url/g, logObj.url);
  format = format.replace(/%status/g, logObj.status);
  format = format.replace(/%elapsedTime/g, `${logObj.elapsedTime}ms`);

  return format;
};

exports.formatError = function(logObj) {
  let format = '<- %requestId %method API %url %status %elapsedTime %message';

  format = format.replace(/%requestId/g, logObj.requestId);
  format = format.replace(/%method/g, logObj.method.toUpperCase());
  format = format.replace(/%url/g, logObj.url);
  format = format.replace(/%status/g, logObj.status);
  format = format.replace(/%elapsedTime/g, `${logObj.elapsedTime}ms`);
  format = format.replace(/%message/g, logObj.message);

  return format;
};

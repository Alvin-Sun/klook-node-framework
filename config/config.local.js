'use strict';

module.exports = appInfo => {
  const config = {};

  config.LOGQUERY_URL = 'https://dev-frontsrv-new.dev.klook.io/v2/frontlogsrv/log/web';

  return config;
};

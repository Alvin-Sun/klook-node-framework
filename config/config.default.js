'use strict';

module.exports = appInfo => {
  const config = {};

  /**
   * some description
   * @member Config#test
   * @property {String} key - some description
   */
  config.test = {
    key: appInfo.name + 'klook egg base',
  };

  config.LOGQUERY_URL = 'https://dev-frontsrv-new.dev.klook.io/v2/frontlogsrv/log/web';
  config.SLACK_ALERT_URL = 'https://hooks.slack.com/services/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';

  return config;
};

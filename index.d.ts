import 'egg'
type Layer = (...args: any[]) => void
import logquery from '@klook/logquery'

interface Klook {
    language: string,
    currency: string,
    [prop: string]: any
}

declare module 'egg' {
    // 扩展 app
    interface Application {
        $logquery: logquery
    }

    interface Context {
        $logError: (info: string, error?: Error) => void,
        $logInfo: (info: string) => void,
        $logger: {
            trace: Layer
            debug: Layer
            info: Layer
            warn: Layer
            error: Layer
            fatal: Layer
        },
        $request: {
            $delete: (url: string, options: any) => Promise<any>
            $get: (url: string, options: any) => Promise<any>
            $head: (url: string, options: any) => Promise<any>
            $options: (url: string, options: any) => Promise<any>
            $post: (url: string, options: any) => Promise<any>
            $put: (url: string, options: any) => Promise<any>
            $patch: (url: string, options: any) => Promise<any>
        },
        $env: {
            isLocal: boolean,
            isProd: boolean
        },
        klook: Klook,
        updateKlook: (data: Klook) => void,
        setKlook: (k: keyof Klook, v: any) => void
    }

    interface IHelper {
        promise: (Promise) => Promise<[Error | null, any]>,
    }
}

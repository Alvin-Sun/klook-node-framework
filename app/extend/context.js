'use strict';
const { urlTemplate, isEmpty, formatReq, formatRes, formatError, isObject } = require('../../share/utils');
const logger = require('../../share/logger');
const v4 = require('uuid').v4;
const querystring = require('querystring');

const REQUEST = Symbol('Context#request');
const REQUESTID = Symbol('Context#REQUESTID');
const KLOOK = Symbol('Context#KLOOK');

module.exports = {
  /**
   * 获取requestId
   * @param  {Context} this egg context
   * @return {String} request 头中的X-Klook-Request-Id
   */
  get requestId() {
    const id = this[REQUESTID];
    if (id) return id;
    this[REQUESTID] = this.get('X-Klook-Request-Id') || v4();
    return this[REQUESTID];
  },

  /**
   * 请求的基础方法，封装egg的this.curl方法
   * @param url 请求地址
   * @param options 请求参数
   * @return {Promise<any>} 透传请求结果，错误都会reject
   */
  async _request(url, options) {
    const startTime = new Date().getTime();
    const { params = {}, query, baseUrl, ...rest } = options;

    let requestUrl = isEmpty(params) ? url : urlTemplate(url, params);

    if (!/^https?:\/\//.test(requestUrl)) {
      if (!baseUrl) {
        this.$logger.error('base url is needed');
        return new Promise.reject(new Error('base url is needed'));
      }

      requestUrl = baseUrl + requestUrl;
    }

    if (query) {
      const queries = querystring.stringify(query);
      requestUrl += ('?' + queries);
    }

    /** *
     * todo
     * [ ]. trace
     * [ ]. 是否需要一些自定义header
     * [x]. 错误处理
     * [x]. 方法支持
     * [ ]. 拆分请求和log
     */

    // 发起请求打印
    this.$logger.info(formatReq({
      requestId: this.requestId,
      method: rest.method,
      url: requestUrl,
    }));

    try {
      const res = await this.curl(requestUrl, {
        contentType: 'json',
        dataType: 'json',
        ...rest,
      });

      // 发起请求成功打印
      this.$logger.info(formatRes({
        requestId: this.requestId,
        method: options.method,
        url: requestUrl,
        status: res.status,
        elapsedTime: Date.now() - startTime,
      }));

      // 业务错误抛错，可以和网络错误一起被调用者catch，做统一处理
      const { data } = res;
      if (isObject(data) && !data.success) {
        return Promise.reject(new Error({ message: 'net work error', ...data }));

      }

      // 透传请求结果
      return data;

    } catch (e) {
      // 发起请求失败上报日志
      this.$logger.error(formatError({
        requestId: this.requestId,
        method: options.method,
        url: requestUrl,
        status: e.status,
        elapsedTime: Date.now() - startTime,
        message: e.message,
      }));

      // 发起请求失败打印日志
      this.$logError('request error', e);

      return Promise.reject(e);
    }
  },

  get $request() {
    if (!this[REQUEST]) {
      const requestMethods = [ 'delete', 'get', 'head', 'options', 'post', 'put', 'patch' ];
      this[REQUEST] = requestMethods.reduce((acc, method) => {
        return {
          ...acc,
          [`$${method}`]: (url, options) => this._request(url, {
            ...options,
            url,
            method,
          }),
        };
      }, {});
    }

    return this[REQUEST];
  },

  // 控制台打印
  get $logger() {
    return logger;
  },

  get $env() {
    return {
      isLocal: this.app.config.env === 'local',
      isProd: this.app.config.env === 'prod',
    };
  },

  $logInfo(message) {
    if (!message) {
      return;
    }

    try {
      this.app.$logquery.service({
        timestamp: +Date.now(),
        level: 'I',
        tag: 'email',
        requestId: this.requestId,
        message,
      });
    } catch (error) {
      // Ignore
    }

    this.$env.isLocal && this.$logger.info(message);
  },

  $logError(info = 'error', error) {
    // 发送至slack
    const { config: { SLACK_ALERT_URL } } = this.app;
    if (this.$env.isProd) {
      const text = `
      \`\`\`
      [klook-email-server error notify]
      -------------------------------------------
      datetime: ${new Date().toString()}
      level: E
      message: ${info}
      \`\`\`
      `;

      this.$request.post(SLACK_ALERT_URL, {
        data: {
          text: text.trim(),
        },
      });
    }

    const message = error ? JSON.stringify({
      errorMessage: `${info}: ${error.message}`,
      errorStack: error.stack,
    }) : info;

    try {
      this.app.$logquery.service({
        timestamp: +Date.now(),
        level: 'E',
        tag: 'email',
        requestId: this.requestId,
        message,
      });
    } catch (error) {
      // Ignore
    }

    this.$env.isLocal && this.$logger.error(message);
  },

  get klook() {
    if (this[KLOOK]) {
      return this[KLOOK];
    }

    this[KLOOK] = {
      language: this.get('accept-language'),
      currency: this.get('currency'),
    };

    return this[KLOOK];
  },

  setKlook(k, v) {
    this.klook && (this[KLOOK][k] = v);
  },

  updateKlook(data) {
    if (!data) { return; }

    for (const i of Object.keys(data)) {
      this.setKlook(i, data[i]);
    }
  },
};

'use strict';
const Logquery = require('@klook/logquery');

const LOGQUERY = Symbol('Application#logquery');

const config = {
  // 日志上报
  get $logquery() {
    if (!this[LOGQUERY]) {
      const { LOGQUERY_URL } = this.config;

      this[LOGQUERY] = new Logquery({
        url: LOGQUERY_URL,
        headers: { 'X-Platform': 'email' },
        queue: { interval: 15000, size: 15 },
      });
    }

    return this[LOGQUERY];
  },
};

module.exports = config;

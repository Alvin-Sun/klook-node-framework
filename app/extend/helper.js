'use strict';

// app/extend/helper.js
module.exports = {
  promise(p) {
    // this 是 helper 对象，在其中可以调用其他 helper 方法
    // this.ctx => context 对象
    // this.app => application 对象
    return new Promise(resolve => {
      p.then(res => {
        resolve([ null, res ]);
      }).catch(error => {
        resolve([ error, null ]);
      });
    });
  },

  formatString(...args) {
    const [ str, ...params ] = args;

    const len = params.length;
    if (!len) {
      return str;
    }

    return params.reduce((acc, v, index) => {
      try {
        const param = typeof v === 'string' ? v : JSON.stringify(v);

        if (index === len - 1) {
          return acc.replace(/%s/g, param);
        }

        return acc.replace('%s', param);
      } catch (e) {
        return acc;
      }

    }, str);
  },
};
